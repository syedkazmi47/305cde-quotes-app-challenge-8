var myApp = angular.module('myApp', ["firebase"]);
myApp.controller('PeopleController', function ($scope, $firebase) {

        $scope.person = {

            author: $scope.author,
            category: $scope.category,
            quotes: [$scope.quote]

        };
    
    var ref = new Firebase("https://quotifier.firebaseio.com/authors");
    
    // create an AngularFire reference to the data
    var sync = $firebase(ref);
    
   // download the data into a local object
    $scope.people = sync.$asArray();
    
    $scope.add = function()
    {
        sync.$push($scope.person);
    }
    
    $scope.update = function()
    {
       
        var ref =  new Firebase("https://quotifier.firebaseio.com/authors/"+ $scope.author +"/quotes/");
        
        var a = $firebase(ref);
        
        //alert($scope.author);
        a.$push($scope.quotes);
    }
});
